# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Sensor fusion  ROS node for position, velocity, bearing and yaw-rate estimation ###

A sensor fusion module for miniature autonomous vehicles based on, visual position as seen form a camera as well as data from an imu. Implemented using an extended kalman filter

Implemented in c++ for ROS using Eigen.

### Prerequsites ###

Eigen, ROS lunar loggerhead

### Topics from ROS ###

subscribes to: car0PoseFromCam, car1PoseFromCam,  imuData0, imuData1

publishes: estState0, estState1
