//... Private header file with prototype for prediction and
//  coordinatedTurnMotion.
#include "../include/vehicle.h"

void Vehicle::prediction(void){
  /*
  prediction() calculates the prediction step in an extended Kalman filter.
  The number of states used is assumed known.

  Since the model used is nonlinear it is implemened
  as an Extended Kalman filter
  https://en.wikipedia.org/wiki/Extended_Kalman_filter#Predict
  */

  // update state vector and retrive the jacobian
  MatrixXf FX(6,6);
  FX = coordinatedTurnMotion();
  // compute and update the covariance matrix
  X_P = FX*X_P*FX.transpose() + (this->Q)*X_DT;
  // since numerical errors can make the covarience matrix non-symmetric
  // symmetry is forced
  this->preserveSymmetry();
}

MatrixXf Vehicle::coordinatedTurnMotion(void){
  /*
  COORDINATEDTURNMOTION calculates the predicted state using a coordinated
  turn motion model, and also calculates the motion model Jacobian.

  The result is a propagation of the vehicle state through time.

  Input:
  x           [6 x 1] state vector
  dt          [1 x 1] Sampling time

  Output:
  fx          [6 x 1] motion model evaluated at state x

  fx =
  px + vx*cos(theta + beta)*dt
  py + vx*sin(theta + beta)*dt
  vx + ax*dt
  ax
  theta + omega*dt
  omega;

  Fx          [6 x 6] motion model Jacobian evaluated at state x

  Fx =
  1   0   dt*cos(theta + beta)  0   -vx*sin(theta + beta)*dt  0
  0   1   dt*sin(theta + beta)  0    vx*cos(theta + beta)*dt  0
  0   0   1                     dt   0                        0
  0   0   0                     1    0                        0
  0   0   0                     0    1                        dt
  0   0   0                     0    0                        1;

  NOTE: the motion model assumes that the state vector x consist of the
  following states:
  px          X-position
  py          Y-position
  vx          local x-velocity
  ax          local x-acceleration
  theta       heading in radians
  omega       turn-rate in radians/second

  Other input:
  beta        slip angle of vehicle where theta + beta is the current
              heading of the vehicle
  *where local describes a coorodinate system
  relative to the car where x is forward
  */

  // the motion model
  //NOTE: the addition of beta is because the heading of the vehicle differs
  // from the angle of the chassis.
  VectorXf fx(6);
  fx(PX) = X_PX + X_DT*X_VX*cos(X_THETA + beta);
  fx(PY) = X_PY + X_DT*X_VX*sin(X_THETA + beta);
  fx(VX) = X_VX + X_AX*X_DT;
  fx(AX) = X_AX;
  fx(THETA) = X_THETA +X_DT*X_OMEGA;
  fx(OMEGA) = X_OMEGA;

  //update the state vector
  this->x = fx;

  //Create the jacobian of the motionmodel evaluated at current state
  MatrixXf FX(6,6);
  FX = Eigen::MatrixXf::Zero(6,6);
  FX(PX,PX) =       1.0;
  FX(PX,VX) =       X_DT*cos(X_THETA + beta);
  FX(PX,THETA) =    X_DT*(-X_VX*sin(X_THETA + beta));
  FX(PY,PY) =       1.0;
  FX(PY,VX) =       X_DT*sin(X_THETA + beta);
  FX(PY,THETA) =    X_DT*( X_VX*cos(X_THETA + beta));
  FX(VX,VX) =       1.0;
  FX(VX,AX) =       X_DT;
  FX(AX,AX) =       1.0;
  FX(THETA,THETA) = 1.0;
  FX(THETA,OMEGA) = X_DT;
  FX(OMEGA,OMEGA) = 1.0;

  return FX;
}


void Vehicle::camUpdate(geometry_msgs::Vector3Stamped cam){

  /*
  camUpdate() calculates mean and covariance of a state
  density using a non-linear Gaussian model, EKF.

  Input:
  x           [6 x 1] Prior mean
  P           [6 x 6] Prior covariance
  cam         [3 x 1] measurement vector [x, y, angle]^T
  Rw_cam      [3 x 3] Measurement noise covariance

  Output:
  x           [8 x 1] updated state mean
  P           [8 x 8] updated state covariance

  NOTE: the state vector x consist of the    following states:
  px          X-position [m]
  py          Y-position [m]
  vx          local x-velocity [m/s]
  ax          local x-acceleration [m/s^2]
  theta       heading [rad]
  omega       turn-rate [rad/s]
  *where local describes a coorodinate system
  relative to the car where x is forward
  */

  // the measurment model
  VectorXf hx(3);
  hx(0) = X_PX; // px
  hx(1) = X_PY; // py
  hx(2) = X_THETA; // theta

  // calculate number of n to 'correct' the angle with
  // needed as the theta of the model belongs to [-inf inf] and the theta from the cam
  // belongs to [0 2*pi]
  if (this->old_theta > M_PI*1.5 && cam.vector.z < M_PI/2.0) {
    (this->n) += 1;
  }
  else if (this->old_theta < M_PI/2.0 && cam.vector.z > M_PI*1.5) {
    (this->n) -= 1;
  }

  // the measurment vector
  VectorXf y(3);
  y = VectorXf::Zero(3);
  y(0) = cam.vector.x;
  y(1) = cam.vector.y;
  y(2) = cam.vector.z + (this->n)*M_PI*2.0;
  (this->old_theta) = cam.vector.z;

  //Create the jacobian of the measurment model evaluated at current state
  MatrixXf HX(3,6);
  HX = MatrixXf::Zero(3,6);
  HX(0,PX) = 1.0;
  HX(1,PY) = 1.0;
  HX(2,THETA) = 1.0;

  // initilize variables
  MatrixXf S(3,3);
  MatrixXf K(6,3);
  VectorXf v(3);

  S = HX*X_P*HX.transpose() + Rw_cam;
  K = X_P*HX.transpose()*S.inverse();
  v = y - hx;

  // update state vector mean
  this->x = this->x + K*v;
  // update state covarience
  X_P = X_P - (K*S*K.transpose());
  // since numerical errors can make the covarience matrix non-symmetric
  // symmetry is forced
  this->preserveSymmetry();
}

void Vehicle::imuUpdate(geometry_msgs::Vector3Stamped imu){
  /*
  imuUpdate() calculates mean and covariance of a state
  density using a non-linear Gaussian model, EKF.

  https://en.wikipedia.org/wiki/Extended_Kalman_filter#Update

  Input:
  x           [6 x 1] Prior mean
  P           [6 x 6] Prior covariance
  imu         [2 x 1] [acc_x, omega]^T
  Rw_imu      [2 x 2] Measurement noise covariance

  Output:
  x           [6 x 1] updated state mean
  P           [6 x 6] updated state covariance

  NOTE: the state vector x consist of the    following states:
  px          X-position [m]
  py          Y-position [m]
  vx          local x-velocity [m/s]
  ax          local x-acceleration [m/s^2]
  theta       heading [rad]
  omega       turn-rate [rad/s]
  *where local describes a coorodinate system
  relative to the car where x is forward
  */

  // the measurment model
  VectorXf hx(2);
  hx(0) = X_AX;
  hx(1) = X_OMEGA;

  // the measurment vector
  VectorXf y(2);
  y(0) = imu.vector.x; // local inertia acceleration x
  y(1) = imu.vector.z; // yaw rate

  //Create the jacobian of the measurment model evaluated at current state
  MatrixXf HX(2,6);
  HX = MatrixXf::Zero(2,6);
  HX(0,AX) = 1.0;
  HX(1,OMEGA) = 1.0;

  // initilize variables
  MatrixXf S(2,2);
  MatrixXf K(6,2);
  VectorXf v(2);

  S = HX*X_P*HX.transpose() + this->Rw_imu;
  K = X_P*HX.transpose()*S.inverse();
  v = y - hx;

  // update state vector mean
  this->x = this->x + K*v;
  // update state covarience
  X_P = X_P - (K*S*K.transpose());
  // since numerical errors can make the covarience matrix non-symmetric
  // symmetry is forced
  this->preserveSymmetry();
}

void Vehicle::timeUpdate(void){
  this->T = ros::Time::now();
  X_DT = this->T.toSec() - this->T_last.toSec();
  this->T_last = this->T;
}

// This functions tries to avoid some of the roundoff errors that may occur
void Vehicle::preserveSymmetry(void){
  X_P = (X_P + X_P.transpose())/2.0;
}

void Vehicle::sanityCheck(geometry_msgs::Vector3Stamped cam){
  /*
  Resets the position to last camera value aswell as resetting the covariance matrix
  if the data from the Kalman filter is suspected to be 'garbage'. E.g. outside
  of area visible by camera, unresonably fast or NaN or +- inf in the covariance
  matrix.
  */
  if ( X_PX < AREA_X_MIN ||
       X_PX > AREA_X_MAX ||
       X_PY < AREA_Y_MIN ||
       X_PY > AREA_Y_MAX ||
       X_VX < MIN_X_VEL  ||
       X_VX > MAX_X_VEL  ||
       //X_OMEGA > MAX_OMEGA ||
       //X_OMEGA < MIN_OMEGA ||
       !X_P.allFinite() )
  {
    X_PX = cam.vector.x;
    X_PY = cam.vector.y;;
    X_VX = 0.0;
    X_AX = 0.0;
    X_THETA = cam.vector.z;
    X_OMEGA = 0.0;

    X_P = MatrixXf::Ones(6,6)*1.0e6;
  }
}
