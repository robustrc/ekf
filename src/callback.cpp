//...function includes
#include "../include/callback.h"

geometry_msgs::Vector3Stamped cam[2];
geometry_msgs::Vector3Stamped imu[2];
float delta = 0.0;
float beta = 0.0;

void camCallback0(const geometry_msgs::Vector3Stamped camData0)
{
  cam[0] = camData0;
}

void camCallback1(const geometry_msgs::Vector3Stamped camData1)
{
  cam[1] = camData1;
}

void imuCallback0(const geometry_msgs::Vector3Stamped imuData0)
{
  imu[0] = imuData0;
  imu[0].vector.x =  imuData0.vector.x*G;
  imu[0].vector.y =  imuData0.vector.y*G;
  imu[0].vector.z =  imuData0.vector.z*DEG2RAD;
}

void imuCallback1(const geometry_msgs::Vector3Stamped imuData1)
{
  imu[1] = imuData1;
}

void controlCallback0(const rc_msgs::ControlSignal control)
{
  /* delta is the wheel angle as given by control input. Beta is then the
     calculated heading of the vehicle.
     Distance from front wheel axis to center of gravity = 0.0525 m
     Distance between front and read wheel axis = 0.102 m */

  delta = ((control.delta-1600.0)/1600.0)*25.0*DEG2RAD;
  beta = beta*0.9 + 0.1*(atan((tan(delta)*0.0525)/0.102));


}
