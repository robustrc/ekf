#ifndef EKF_NODE_H
#define EKF_NODE_H

//... Standard includes
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
using namespace std;

//...Eigen include
#include "/usr/include/eigen3/Eigen/Dense"
using namespace Eigen;

//...ROS includes
#include "/opt/ros/lunar/include/ros/ros.h"
#include "/opt/ros/lunar/include/tf/tf.h"
#include "/opt/ros/lunar/include/geometry_msgs/Vector3Stamped.h"
#include "/opt/ros/lunar/include/geometry_msgs/AccelStamped.h"

#include <rc_msgs/StateStamped.h>

#include <boost/algorithm/string.hpp>

//...function includes
#include "../include/vehicle.h"
#include "../include/callback.h"


#define RAD2DEG 180.0/M_PI


#endif
