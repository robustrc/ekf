#include "ekf_node.h"

int main( int argc, char** argv ) {

  //Define some options (all lowercase)
  string optDebug[2] = {"d", "debug"};
  bool debug = false;
  string optNoCam[2] = {"nc", "nocam"};
  bool nocam = false;
  string optNoImu[2] = {"ni", "noimu"};
  bool noimu = false;


  if( argc > 1 ) {
    string arg[argc -1];
    for (size_t i = 0; i < argc - 1; i++) {
      arg[i] =  string(argv[i+1]);
      boost::algorithm::to_lower(arg[i]);

      if( arg[i] == optDebug[0] || arg[i] == optDebug[1]) {
        debug = true;
        printf("Debug output enabled.\n");
      }
      else if( arg[i] == optNoCam[0] || arg[i] == optNoCam[1]) {
        nocam = true;
        printf("Camera input disabled.\n");
      }
      else if( arg[i] == optNoImu[0] || arg[i] == optNoImu[1]) {
        noimu = true;
        printf("Imu input disabled.\n");
      }
      else {
        printf("'%s' is not a recognised argument. Parhaps you have made a typo?\n", argv[i+1]);
      }
    }
  }

  bool runonce = true;
  bool initdone = false;

  int NrOfCars = 2; // hard coded for now

  // Init ROS
  ros::init(argc, argv, "EKF");
  ros::NodeHandle n;
  // rate in Hz
  ros::Rate loop_rate(500);

  // Initilize parameters from parameterfile
  // creates two cars
  #include "parameters.h"

  //--------------------------------------------------------------------------
  // Create publisher
  ros::Publisher stateVectorPub0 = n.advertise<rc_msgs::StateStamped>("estState0", 1);
  ros::Publisher stateVectorPub1 = n.advertise<rc_msgs::StateStamped>("estState1", 1);

  rc_msgs::StateStamped state[2];

  // subscribe to topic published by camera.
  ros::Subscriber subCam0 = n.subscribe("car0PoseFromCam", 1, camCallback0);
  ros::Subscriber subCam1 = n.subscribe("car1PoseFromCam", 1, camCallback1);
  // subscribe to topic published by imu.
  ros::Subscriber subIMU0 = n.subscribe("imuData0", 1, imuCallback0);
  ros::Subscriber subIMU1 = n.subscribe("imuData1", 1, imuCallback1);

  ros::Subscriber subCTRL = n.subscribe("control", 1, controlCallback0);

  while (ros::ok()) {
    // Wait for published topcis to initilize and get a "first" position
    if (runonce) {
      // wait for a message from topic car0PoseFromCam to set initial values
      boost::shared_ptr<geometry_msgs::Vector3Stamped const> camMsgRecived;
      camMsgRecived = ros::topic::waitForMessage<geometry_msgs::Vector3Stamped>("car0PoseFromCam",ros::Duration(1));
      if (camMsgRecived == NULL){
        ROS_INFO("No pose information from camera received");
      }

      // wait for a message from topic imuData0 to set initial values
      boost::shared_ptr<geometry_msgs::Vector3Stamped const> imuMsgRecived;
      imuMsgRecived = ros::topic::waitForMessage<geometry_msgs::Vector3Stamped>("imuData0",ros::Duration(1));
      if (imuMsgRecived == NULL){
        ROS_INFO("No IMU information received");
      }

      if (!nocam && camMsgRecived != NULL && cam[0].header.seq > 0) {
        for (size_t i = 0; i < NrOfCars; i++) {
          Car[i].x(PX) = cam[i].vector.x;
          Car[i].x(PY) = cam[i].vector.y;
          Car[i].x(THETA) = cam[i].vector.z;
          Car[i].seqCam = cam[i].header.seq;
          Car[i].seqImu = imu[i].header.seq;

          ROS_INFO_STREAM("Initial positions for car nr " << i << ", set to: " << Car[i].x(PX) << ", " << Car[i].x(PY));
        }
        initdone = true;
        runonce = false;
      }
      else if (nocam && imuMsgRecived != NULL && imu[0].header.seq > 0) {
        for (size_t i = 0; i < NrOfCars; i++) {
          Car[i].x(PX) = 1.5;
          Car[i].x(PY) = 1;
          Car[i].x(THETA) = 0;
          Car[i].seqCam = 0;
          Car[i].seqImu = imu[i].header.seq;

          ROS_INFO_STREAM("Initial positions for car nr " << i << ", set to: " << Car[i].x(PX) << ", " << Car[i].x(PY));
        }
        initdone = true;
        runonce = false;
      }
    }
    if (initdone) {
      //--------------------------------------------------------------
      for (size_t i = 0; i < NrOfCars; i++)
      {
        // Prediction
        Car[i].timeUpdate();
        Car[i].prediction();
        if (debug && false) {
          ROS_INFO_STREAM("Prediction for car nr: " << i );
        }

        // Update using camera mesurement
        if (!nocam && Car[i].seqCam < cam[i].header.seq) {
          Car[i].seqCam = cam[i].header.seq;

          Car[i].camUpdate(cam[i]);
          if (debug) {
            ROS_INFO_STREAM("Updated by cam, car nr: " << i);
          }
        }

        // Update using imu measurment
        if (!noimu && Car[i].seqImu < imu[i].header.seq) {
          Car[i].seqImu = imu[i].header.seq;

          Car[i].imuUpdate(imu[i]);
          if (debug) {
            ROS_INFO_STREAM("Updated by IMU, car nr: "  << i);;
          }
        }

        Car[i].sanityCheck(cam[i]);

        // Publish calculated data
        state[i].header.stamp = ros::Time::now();
        state[i].px = Car[i].x(PX);
        state[i].py = Car[i].x(PY);
        state[i].vx = Car[i].x(VX);
        state[i].ax = Car[i].x(AX);
        state[i].theta = Car[i].x(THETA);
        state[i].omega = Car[i].x(OMEGA);

        if (i == 0)
        {
          state[i].header.frame_id = "0";
          stateVectorPub0.publish(state[i]);
        }
        else if (i == 1)
        {
          state[i].header.frame_id = "1";
          stateVectorPub1.publish(state[i]);
        }
      }
    }
    ros::spinOnce();
    loop_rate.sleep();
    //-------------------------------------
  }
  return 0;
}
