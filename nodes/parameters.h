#ifndef PARAMETERS_H
#define PARAMETERS_H

#include "ekf_node.h"

Vehicle Car[2];

// Initilize CarOne
//--------------------------------------------------------------------------
//Create Car one
Vehicle car0;
Car[0] = car0;
// State vector
Car[0].x = VectorXf::Zero(6);

// State covariance matrix
Car[0].P = MatrixXf::Ones(6,6)*1.0e6;

// Process noise covariance matrix
Car[0].Q = MatrixXf::Zero(6, 6);
Car[0].Q(PX,PX) = 0;
Car[0].Q(PY,PY) = 0;
Car[0].Q(VX,VX) = 0;
Car[0].Q(AX,AX) = pow(0.5,2);
Car[0].Q(THETA,THETA) = 0;
Car[0].Q(OMEGA,OMEGA) = pow(180.0*DEG2RAD,2);

// Camera noise covariance matrix
Car[0].Rw_cam = MatrixXf::Zero(3, 3);
Car[0].Rw_cam <<  pow(5.0e-4,2),   0.0,      0.0,
                  0.0,    pow(5.0e-4,2),     0.0,
                  0.0,             0.0,   pow(1*DEG2RAD,2);

Car[0].old_theta = 0.0;
Car[0].n = 0;

// imu noise covariance
Car[0].Rw_imu = MatrixXf::Zero(2, 2);
Car[0].Rw_imu <<   1.0e-2,   0,
                   0,    1.0e-2;

Car[0].T = ros::Time::now();
Car[0].T_last = ros::Time::now();

Car[0].seqCam = 0;
Car[0].seqImu = 0;
//--------------------------------------------------------------------------

// Initilize CarTwo
Vehicle car1;
Car[1] = car1;
//--------------------------------------------------------------------------
// State vector
Car[1].x = VectorXf::Zero(6);

// State covariance matrix
Car[1].P = MatrixXf::Ones(6,6)*1.0e6;

// Process noise covariance matrix
Car[1].Q = MatrixXf::Zero(6, 6);

// Camera noise covariance matrix
Car[1].Rw_cam = MatrixXf::Zero(3, 3);
Car[1].Rw_cam(0,0) = 0.1;
Car[1].Rw_cam(1,1) = 0.1;
Car[1].Rw_cam(2,2) = 0.2;

Car[1].old_theta = 0.0;
Car[1].n = 0;

// imu noise covariance
Car[1].Rw_imu = MatrixXf::Zero(2, 2);
Car[1].Rw_imu(0,0) = 0.1;
Car[1].Rw_imu(1,1) = 0.1;

Car[1].T = ros::Time::now();
Car[1].T_last = ros::Time::now();

Car[1].seqCam = 0;
Car[1].seqImu = 0;
//--------------------------------------------------------------------------


#endif
