#ifndef CAR_H
#define CAR_H

//... Standard includes
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
using namespace std;

//...Eigen include
#include "/usr/include/eigen3/Eigen/Dense"
using namespace Eigen;

//...ROS includes
#include "/opt/ros/lunar/include/ros/ros.h"
#include "/opt/ros/lunar/include/tf/tf.h"
#include "/opt/ros/lunar/include/geometry_msgs/Vector3Stamped.h"
#include "/opt/ros/lunar/include/geometry_msgs/AccelStamped.h"

#include <boost/algorithm/string.hpp>
#include <boost/math/special_functions/sign.hpp>

#define PX 0
#define PY 1
#define VX 2
#define AX 3
#define THETA 4
#define OMEGA 5

#define X_PX (this->x)(PX)
#define X_PY (this->x)(PY)
#define X_VX (this->x)(VX)
#define X_AX (this->x)(AX)
#define X_THETA (this->x)(THETA)
#define X_OMEGA (this->x)(OMEGA)
#define X_DT (this->dt)

#define X_P (this->P)

#define AREA_X_MIN 0.0
#define AREA_X_MAX 4.29
#define AREA_Y_MIN 0.0
#define AREA_Y_MAX 3.43
#define MAX_X_VEL 5.0
#define MIN_X_VEL -5.0
#define MAX_OMEGA 6.0
#define MIN_OMEGA -6.0


class Vehicle{
public:
  VectorXf x;
  MatrixXf P;
  MatrixXf Q;
  MatrixXf Rw_cam;
  MatrixXf Rw_imu;
  ros::Time T;
  ros::Time T_last;
  double dt;

  int seqCam;
  float old_theta;
  int n;
  int seqImu;

  void prediction(void);
  MatrixXf coordinatedTurnMotion(void);
  void camUpdate(geometry_msgs::Vector3Stamped cam);
  void imuUpdate(geometry_msgs::Vector3Stamped imu);
  void timeUpdate(void);
  void preserveSymmetry(void);
  void sanityCheck(geometry_msgs::Vector3Stamped cam);
};
//...function includes
#include "../include/callback.h"

#endif
