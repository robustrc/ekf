#ifndef CALLBACK_H
#define CALLBACK_H

//... Standard includes
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
using namespace std;

//...Eigen include
#include "/usr/include/eigen3/Eigen/Dense"
using namespace Eigen;

//...ROS includes
#include "/opt/ros/lunar/include/ros/ros.h"
#include "/opt/ros/lunar/include/tf/tf.h"
#include "/opt/ros/lunar/include/geometry_msgs/Vector3Stamped.h"
#include "/opt/ros/lunar/include/geometry_msgs/AccelStamped.h"
#include <rc_msgs/ControlSignal.h>

#define RAD2DEG 180.0/M_PI
#define DEG2RAD M_PI/180.0
#define G 9.81

//.. Global variables
extern geometry_msgs::Vector3Stamped cam[2];
extern geometry_msgs::Vector3Stamped imu[2];
extern float beta;

void camCallback0(const geometry_msgs::Vector3Stamped camData0);

void camCallback1(const geometry_msgs::Vector3Stamped camData1);

void imuCallback0(const geometry_msgs::Vector3Stamped imuData0);

void imuCallback1(const geometry_msgs::Vector3Stamped imuData1);

void controlCallback0(const rc_msgs::ControlSignal control);

#endif
